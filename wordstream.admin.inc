<?php

/**
 * @file
 * Admin include file.
 */

/**
 * Displays the form for the standard settings tab.
 *
 * @return
 *   array A structured array for use with Forms API.
 */
function wordstream_admin_settings() {
  $wordstream = wordstream_include_api_class();
  if(!$wordstream) {
    return ''; 
  }
  $ret = $wordstream->getAPICredits();
  $account = 1;
  if(!$ret->credits_per_month) {
    drupal_set_message(t('You are unable to log in to WordStream. Please enter a valid WordStream API username and password.'), 'error');
    $account = 0;
  }
//dsm($ret);
  
  $form['wordstream_username'] = array(
    '#type' => 'textfield',
    '#title' => t('WordStream API username'), 
    '#description' => t(''),
    '#default_value' => variable_get('wordstream_username', ''), 
    '#description' => t('In order to enable the API tools you will need a WordStream account login (username & password). !wordstream_link.',
      array(
        '!wordstream_link' => l(t('Get your API account here'), WORDSTREAM_LINK_API_ACCOUNT, array('attributes' => array('target', 'wordstream'))),
      )
    )  
  );
  $form['wordstream_password'] = array(
    '#type' => 'password',
    '#title' => t('WordStream API password'), 
    '#description' => t(''),
    '#default_value' => variable_get('wordstream_password', ''), 
  );
  
  $form['wordstream_free_embed_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Free widgets embed id'), 
    '#description' => t(''),
    '#default_value' => variable_get('wordstream_free_embed_id', ''), 
    '#description' => t('In order to enable the free javascript tools you will need a WordStream widget embed id. The embed id is a part of the widget embed code that you can generate on the WordStream site. Enter in the field above the 20 character value following "embed_id=" in the widget embed code. !wordstream_link.',
      array(
        '!wordstream_link' => l(t('Get your embed code here'), WORDSTREAM_LINK_WIDGET_GENERATOR, array('attributes' => array('target', 'wordstream'))),
      )
    )    
  );

  $form['wordstream_adult_filter_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Adult keyword filter default'),
    '#return_value' => 'on',
    '#default_value' => variable_get('wordstream_adult_filter_default', TRUE),
    '#description' => t('Set the default value adult filter for WordStream report values.'),
  );
  
  $form['wordstream_cache_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache time'), 
    '#description' => t('API results are cached. Use this value to set in seconds the time before checking if the cache data is out of date.'),
    '#default_value' => variable_get('wordstream_cache_time', 604800), 
  );

  if (!$account) {
    return system_settings_form($form);
  }
  $form['wordstream_account_status'] = array(
    '#type' => 'markup',
    '#value' => '<h3>' . t('Account Status') . '</h3>', 
  );
  $form['wordstream_remaining_monthly_credits'] = array(
    '#type' => 'item',
    '#title' => t('Remaining montly credits'), 
    '#value' => $ret->remaining_monthly_credits,
  );
  $form['wordstream_credits_per_month'] = array(
    '#type' => 'item',
    '#title' => t('Credits per month'), 
    '#value' => $ret->credits_per_month,
  );
  
  return system_settings_form($form);
}

function wordstream_free_keyword_tool_page($keyword = NULL) {
  if(!$embed_id = variable_get('wordstream_free_embed_id', '')) {
    drupal_set_message(
      t('You will need a embed id before you can use the free keyword tool. !admin_link',
        array(
          '!admin_link' => l(t('Configure setting here'), 'admin/settings/wordstream'),
        )
      ), 'error');
    $output = '';
  }
  else {
    $output = '
      <iframe width="673" height="557" scrolling="no" frameborder=0 src="http://freekeywordtool.wordstream.com/themes/keyword_tool_widget_big.html?name=Test&color_b=#01679a&color_t=#ffffff&embed_id=' . $embed_id . '"></iframe>
      <br/><a href="http://www.wordstream.com">Powered by WordStream Internet Marketing Software</a>
    ';
  }
  return $output;
}

function wordstream_free_keyword_niche_page($keyword = NULL) {
  if(!$embed_id = variable_get('wordstream_free_embed_id', '')) {
    drupal_set_message(
      t('You will need a embed id before you can use the free keyword tool. !admin_link',
        array(
          '!admin_link' => l(t('Configure setting here'), 'admin/settings/wordstream'),
        )
      ), 'error');
    $output = '';
  }
  else {
    $output = '
    <iframe width="673" height="557" scrolling="no" frameborder=0 src="http://nichefinder.wordstream.com/themes/niche_finder_widget_big.html?name=Test&color_b=#01679a&color_t=#ffffff&embed_id=' . $embed_id . '"></iframe>
    <br/><a href="http://www.wordstream.com">Powered by WordStream Internet Marketing Software</a>
    ';
  }
  return $output;
}

